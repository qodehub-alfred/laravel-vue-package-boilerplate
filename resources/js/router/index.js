import Vue from 'vue'
import Router from 'vue-router'
import  routes from './routes'

Vue.use(Router)

console.log(window.Globals)

const router = new Router({
    routes: routes,
    mode: 'history',
    base: '/' + Globals.path + '/',
})

export default router

