import { 
    App,
    Case
} from '../screens/index.js'

export default [
    { 
        path: '/', 
        redirect: '/app' 
    },
    {
        path: '/app',
        name: 'app',
        component: App,
        meta: {
            needsAuth: false
        },
    },
    {
        path: '/case',
        name: 'case',
        component: Case,
        meta: {
            needsAuth: false
        },
    },
]

// export default as routes