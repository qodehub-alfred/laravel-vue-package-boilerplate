import Vue from 'vue'

export const App = Vue.component('app', require('./App.vue').default);
export const Case = Vue.component('case', require('./Case.vue').default);