<?php

use Illuminate\Support\Facades\Route;

// Catch-all route...
Route::get('/{view?}', 'HomeController@index')
    ->where('view', '(.*)')
    ->name('qoehub_tickets.index');