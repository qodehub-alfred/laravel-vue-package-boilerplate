<?php

namespace Qodehub\TicketingApp\Tests\Feature;

use Qodehub\TicketingApp\RequestTopic;
use Qodehub\TicketingApp\Tests\TestCase;

class InitialTest extends TestCase
{
    public function test_it_is_true()
    {
        $this->assertTrue(true);
    }

    public function test_a_topic_can_be_created_with_the_factory()
    {
        $topic = factory(RequestTopic::class)->create();

        $this->assertCount(1, RequestTopic::all());
    }
}