<?php

namespace Qodehub\TicketingApp;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class QodehubTicketServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()){
            $this->registerPublishing();
        }

        $this->registerResources();
        $this->registerRoutes();
        $this->assetPublishing();
    }

    public function register()
    {
        $this->commands([
            Console\AssetsCommand::class,
        ]);
    }

    private function registerResources()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'ticket');
    }

    protected function registerPublishing()
    {
        $this->publishes([
            __DIR__.'/../config/qodehub.tickets.php' => config_path('qodehub.tickets.php'),
        ], 'qodehub.tickets.config');
    }

    protected function registerRoutes()
    {
        Route::group($this->routeConfiguration(), function() {
            $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        });
    }

    private function routeConfiguration()
    {
        return [
            'prefix' => config('qodehub.tickets.url_prefix.user-app'),
            'namespace' => 'Qodehub\TicketingApp\Http\Controllers'
        ];
    }

    private function assetPublishing()
    {
        $this->publishes([
            __DIR__.'/../public/vendor/qodehub-tickets' => public_path('vendor/qodehub-tickets'),
        ], 'qodehub.tickets.assets');
    }
}