<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Qodehub\TicketingApp\RequestTopic;

$factory->define(RequestTopic::class, function (Faker $faker) {
    return [
        'title' => $title = $faker->word,
        'slug' => Str::slug($title)
    ];
});